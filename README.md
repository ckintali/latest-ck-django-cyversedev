# BioViz *Connect*

BioViz *Connect* is a web application that connects CyVerse Discovery Environment and Integrated Genome Browser (IGB) using the CyVerse Terrain API. Use BioViz *Connect* to add IGB specific metadata such as genome version or foreground color to CyVerse files. Stream files from CyVerse to IGB for visualization. Or create all new IGB visualizations in BioViz *Connect* such as scaled coverage graphs. 

Log in to BioViz [here](https://www.bioviz.org/connect.html).

Read about how BioViz Connect works in the preprint on [bioRxiv](https://biorxiv.org/cgi/content/short/2020.05.15.098533v1).

## Info

### Metadata

Genome Version: Genomes and versions available in IGB. When added to a file, will cause the genome to be loaded when View in IGB is clicked.

IGB Track Color: The foreground/background color appearance for a track when viewed in IGB.

IGB Track Name: The name that will appear as the track label in IGB.

Comments: Additional comments (not viewable in IGB).

### Analyse

Make coverage graph: Make a coverage graph file in bedgraph format from a bam sequence alignment file using bedtools genomecov version 2.26.0 with option -split. See bedtools.readthedocs.io.

Make scaled coverage graph: Make a scaled coverage graph file in bedgraph format from a bam sequence alignment file using deepTools bamCoverage version 3.3.0 with options --normalizeUsing CPM --binSize 1. See deeptools.readthedocs.io.

Find enriched sequence motifs: Find over-represented sequences in a fasta file using DREME from meme-suite.org. DREME discovers short, ungapped motifs that are relatively enriched in your sequences compared with shuffled sequences.

### Manage Link

Make files or folders public/private. Public files are viewable to anyone with the link, including non-CyVerse users. Making a folder public makes all files/folders contained in that folder public. 

## FAQ

### What is BioViz Connect?
BioViz Connect is an all new web application created by the BioViz team for annotating, analyzing and visualizing CyVerse Discovery Environment data in IGB.

### What can I do with BioViz Connect?
View your CyVerse Discovery Environment in an all new dashboard-style application. Stream data to IGB for visualization or use cloud computing to create all new visualizations. Add IGB specific metadata such as genome version and track color to control how data will look once loaded in IGB. Create public links to share and collaborate.

### How do I log in to BioViz Connect?
Use your CyVerse Discovery Environment credentials to log in [here](https://www.bioviz.org/connect.html).

### How do I get a CyVerse Discovery Environment account? 
Create a free CyVerse Discovery Environment account [here](https://user.cyverse.org/register).

### What is CyVerse?
CyVerse is funded by the National Science Foundation with a mission to design, deploy, and expand national Cyberinfrastructure for Life Sciences research.

### What is the Discovery Environment?
The Discovery Environment provides a web interface for cloud based computing and data storage.

## How it works

BioViz *Connect* accesses CyVerse storage and compute resources by calling Terrain API endpoints. BioViz *Connect* forwards those resources to Integrated Genome Browser by hitting local REST API endpoints within Integrated Genome Browser.

BioViz *Connect* consists of a JavaScript-based user interface and a python3 Django back-end that manages authentication and communication with Terrain API endpoints. BioViz *Connect* is deployed on Amazon Web Services infrastructure using the Apache Web server software, but its design is platform agnostic and could run on other cloud or local systems as needed. BioViz *Connect*.

## Contributors

BioViz *Connect* was designed by Dr. Ann Loraine and implemented by Dr. Nowlan Freese, Karthik Raveendran, Chaitanya Kintali, Srishti Tiwari, and Pawan Bole. The BioViz team would also like to thank Paul Sarando, Sarah Roberts, Ian McEwen, Ramona Walls, and Reetu Tuteja for their assistance with the Terrain API and publishing CyVerse apps, which was made possible through CyVerse’s External Collaborative Partnership program.

## Questions?

Email [Dr. Nowlan Freese](nfreese@uncc.edu) or [Dr. Ann Loraine](aloraine@uncc.edu).
