import requests
from .. import settings


def getAccessToken(code):
    r = requests.post(settings.ACCESS_TOKEN_URL,
                      headers={'Content-Type': 'application/x-www-form-urlencoded'},
                      data={'grant_type': 'authorization_code', 'code': code, 'redirect_uri': settings.REDIRECT_URI,
                            'client_id': settings.CLIENT_ID, 'client_secret': settings.CLIENT_SECRET})
    access_token = r.text.split('&')[0].split('=')[1]
    settings.TOKEN_EXPIRY = r.text.split('&')[1].split('=')[1]

    return access_token

