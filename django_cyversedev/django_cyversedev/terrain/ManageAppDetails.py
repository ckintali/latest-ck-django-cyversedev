import requests
from .. import models
import json
import uuid
from ..terrain_model.CustomEncoder import CustomEncoder

def DeleteAppData():
    models.Arguments.objects.all().delete()
    models.InputDetails.objects.all().delete()
    models.AppDetails.objects.all().delete()
    models.igbfileformats.objects.all().delete()


def getLatestApps(accesstoken):
    custom_encoder = CustomEncoder()
    DeleteAppData()
    req_url = 'https://de.cyverse.org/terrain/apps/communities/iplant:de:prod:communities:Integrated Genome Browser/apps'
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    apps = add_IGBApps(r.json(), accesstoken)
    add_igbfileformats(accesstoken)
    return custom_encoder.encode(apps)

def add_igbfileformats(accesstoken):
    files_supported = 'cnchp,lohchp,bar,gff3,gtf,bgn,bgr,bp1,bps,brpt,brs,bsnp,chp,cnt,cyt,ead,fa,fas,fasta,fna,fsa,mpfa,fsh,gb,gen,gr,link.psl,bnib,sin,egr,egr.txt,map,cn_segments,loh_segments,sgr,2bit,useq,var,wig,bdg,bedgraph,vcf,sam,bed,gff,psl,psl3,pslx,bam,axml,bigbed,bb,bw,bigWig,bigwig,das,dasxml,das2xml,narrowPeak,narrowpeak,broadPeak,broadpeak,tally,gz'
    files_supported_list = str.split(files_supported, ',')
    for filetype in files_supported_list:
        fileformat = models.igbfileformats()
        fileformat.id = str(uuid.uuid4())
        fileformat.filetype  = filetype
        fileformat.save()

def formatFileTypes_fromdescription(descrption):
    start_indexFileType = descrption.find('%INPUT%')
    if start_indexFileType == -1:
        return ""
    return descrption[start_indexFileType+7:]

def get_InputDetailsForApp(appId, accesstoken):
    req_url = 'https://de.cyverse.org/terrain/apps/de/'+appId
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    return r.json()

def getCompatibleAppsByformat(Format):
    custom_encoder = CustomEncoder()
    appdetails = list(models.AppDetails.objects.filter(Formats__contains=Format))
    return custom_encoder.encode(appdetails)

def generateHTMLdatafromFormType(inputdetails, parameter):
    htmldata = ''
    FileInput = "<div class='list-group-item' id='{}'><label for='formGroupExampleInput'>{}<i class='fas fa-question-circle' href='#' data-toggle='popover' title='{}'></i></label><input type='text' class='form-control' placeholder='Select a file' value='{}'></div>"
    FileInput_Primary = "<div class='list-group-item' id='{}'><label for='formGroupExampleInput'>{}<i class='fas fa-question-circle' href='#' data-toggle='popover' title='{}'></i></label><input type='text' id='primaryinput' class='form-control' placeholder='Select a file' value='{}'></div>"

    TextSelection = "<div class='list-group-item' id='{}'><i class='fas fa-question-circle mb-2' href='#' data-toggle='popover' title='Popover Header (Variable)' data-content='{}'></i><div class='dropdown'><button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenu' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>{}</button><div class='dropdown-menu' aria-labelledby='dropdownMenu'>{}</div></div></div>"
    dropdown_options = "<button class='dropdown-item' id = '{}' type='button'>{}</button>"
    Integer_html = "<div class='list-group-item' id='{}'><label for='formGroupExampleInput'>{}<i class='fas fa-question-circle' href='#' data-toggle='popover' title='Popover Header (Variable)' data-content={}></i></label><input type='number' class='form-control' placeholder='Example input'></div>"

    if (inputdetails.Formtype=='FileInput' or inputdetails.Formtype == 'FileOutput' or inputdetails.Formtype == 'FolderOutput'):
        if 'defaultValue' in parameter:
            if '*' in parameter['description']:
                htmldata = FileInput_Primary.format(inputdetails.id, inputdetails.label, inputdetails.Description, parameter['defaultValue'])
            else:
                htmldata = FileInput.format(inputdetails.id, inputdetails.label, inputdetails.Description, parameter['defaultValue'])
        else:
            if '*' in parameter['description']:
                htmldata = FileInput_Primary.format(inputdetails.id, inputdetails.label, inputdetails.Description, '')
            else:
                htmldata = FileInput.format(inputdetails.id, inputdetails.label, inputdetails.Description, '')

    elif(inputdetails.Formtype == 'Integer'):
        htmldata = Integer_html.format(inputdetails.id, inputdetails.label, inputdetails.Description)
    elif(inputdetails.Formtype == 'TextSelection'):
        if len(parameter['arguments'])>0:
            dropdown_html_data = ''
            for each_argument in parameter['arguments']:
                dropdown_html_data=dropdown_html_data+dropdown_options.format(str(each_argument['id']), each_argument['display'])
            htmldata = TextSelection.format(inputdetails.id, inputdetails.Description, inputdetails.label, dropdown_html_data)
    return htmldata

def Add_InputDetailsForApp(AppId, accesstoken):
    app_description = get_InputDetailsForApp(AppId, accesstoken)
    for group in app_description['groups']:
        label = group['label']
        for parameter in group['parameters']:
            inputdetails = models.InputDetails()
            inputdetails.Description = parameter['description']
            inputdetails.Formtype = parameter['type']
            inputdetails.AppId = AppId
            inputdetails.id = parameter['id']
            inputdetails.label = parameter['label']
            inputdetails.Inputtype = label
            inputdetails.htmldata = generateHTMLdatafromFormType(inputdetails, parameter)
            inputdetails.save()
            if len(parameter['arguments'])>0:
                for argument in parameter['arguments']:
                    arguments = models.Arguments()
                    arguments.id = argument['id']
                    arguments.isDefault = bool(argument['isDefault'])
                    arguments.Name = argument['name']
                    arguments.value = argument['value']
                    arguments.Display = argument['display']
                    arguments.InputDetailId = inputdetails.id
                    arguments.save()

def add_IGBApps(json, accesstoken):
    apps = []
    for app_json in json['apps']:
        app = models.AppDetails()
        app.id = app_json['id']
        app.Name = app_json['name']
        app.description = app_json['description']
        app.Formats = formatFileTypes_fromdescription(app_json['description'])
        app.save()
        apps.append(app)
        Add_InputDetailsForApp(app.id, accesstoken)
    return apps

def getHtmlDataForAppId(AppId):
    HtmlData = '{\"appelements\":['
    inputfields =models.InputDetails.objects.filter(AppId=AppId)
    for num,field in enumerate(inputfields,start=0):
        if(num==(len(inputfields)-1)):
            HtmlData = HtmlData + "{\"id\":\""+field.id+"\",\"type\":\""+field.Formtype+"\",\"html\":\""+field.htmldata+"\"}"
        else:
            HtmlData = HtmlData + "{\"id\":\""+field.id+"\",\"type\":\""+field.Formtype+"\",\"html\":\""+field.htmldata+"\"},"
    HtmlData=HtmlData+"]}"
    return HtmlData

def getNameForApp(AppId):
    appname = models.AppDetails.objects.filter(id=AppId)[0]
    return appname.Name



