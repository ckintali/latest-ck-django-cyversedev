function createFolderModal(){
   $('#newFolder span').html("");
   $('#newFolder').data("fileInfo",location.hash.replace('#',""))
   $('#newFolder').modal('show');
   $('#newFolder .newFolderTitle').val("Untitled Folder");
}
function createNewFolder(element){
    $('#newFolder span').html("");
    element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"
    var originalPath = $("#newFolder").data("fileInfo");
    var jsonPath = {}

    //var test1 = uriEncodeFileName(originalPath + "/" +$('#newFolder .newFolderTitle').val(),1)
    jsonPath['path']= originalPath+"/"+encodeURIComponent($('#newFolder .newFolderTitle').val())

    $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(jsonPath),
		url: location.origin+"/createFolder/",
	    success: function (data, status, jqXHR) {
	        element.innerHTML="Create";
            location.reload();
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
	        $('#newFolder span').html(JSON.parse(jqXHR.responseText)['message']);
	        element.innerHTML="Create";
	    }
    });
}
function renameFileFolderModal(){
    $('#renameFileFolder').data("fileInfo",$("#context-menu").data("fileInfo"))
    $('#renameFileFolder .renameText').val($('#renameFileFolder').data("fileInfo").label);
    $('#renameFileFolder').modal('show');
    if($("#context-menu").data("fileType") == "folder"){
         $('#renameFileFolder .renameFolderWarning').removeClass("d-none");
         $('#renameFileFolder .renameFolderWarning').addClass("d-block");
    }else{
        $('#renameFileFolder .renameFolderWarning').removeClass("d-block");
        $('#renameFileFolder .renameFolderWarning').addClass("d-none");
    }
}
function renameFileFolder(element){
    element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"

    var originalPath = $("#context-menu").data("fileInfo").path
    var parentPath =originalPath.substring(0,originalPath.lastIndexOf("/"));
    var json = {}
    json['source']= uriEncodeFileName($("#context-menu").data("fileInfo").path);
    json['dest']= uriEncodeFileName(parentPath+"/"+$('#renameFileFolder .renameText').val())
    $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(json),
		url: location.origin+"/renameFileFolder/",
	    success: function (data, status, jqXHR) {
            location.reload();
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
            $('#renameFileFolder .renameError').html(JSON.parse(jqXHR.responseText)['message']);
            element.innerHTML="Save Changes";
	    }
    });
}
function copy_Path(){
    var cpyPth=$("#context-menu").data("fileInfo").label;
    var tempInput = document.createElement('INPUT');
    $("body").append(tempInput);
    tempInput.setAttribute('value',cpyPth)
    tempInput.select();
    document.execCommand('copy');
    $(tempInput).remove();
}
function uploadByURLUI(){
   $('#uploadURL span').html("");
   $('#uploadURL').data("fileInfo",location.hash.replace('#',""))
   $('#uploadURL').modal('show');
}
function uploadFileFolder(){
   $('#uploadFileFolder span').html("");
   $('#uploadFileFolder').data("fileInfo",location.hash.replace('#',""))
   $('#uploadFileFolder').modal('show');
}
function upload(){
        var data = new FormData();
        // Get form
        var form = $('#uploadFileFolderInput')[0].files[0];
        data.append('file', form)
		// disabled the submit button
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: location.origin+"/uploadFile/",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                $("#result").text(data);
                console.log("SUCCESS : ", data);
            },
            error: function (e) {
                if (e.status == 200){
                    $("#result").text(data);
                    console.log("SUCCESS : ", data);
                }
                else{
                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                    $(".uploadFileFolderError").html("ERROR : "+ e.statusText)
                 }
            }
        });
}
function downloadFile(){
	var accesstoken = ""
	var path = $("#context-menu").data("fileInfo").path;
	var filename = $("#context-menu").data("fileInfo").label;
	$.ajax({
		method: "GET",
        dataType: "json",
        async: "true",
		url: location.origin+"/getAccessTokenForDownload/",
	    success: function (data, status, jqXHR) {
            $(".downloadToast").toast("show");
            accesstoken = data["accesstoken"]
            downloadFile_client(path,accesstoken,filename);
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
	    }
    });
}
function downloadFile_client(path, accesstoken, filename){
	xhttp = new XMLHttpRequest();
	url = 'https://de.cyverse.org/terrain/secured/fileio/download?path='+path
	bearer_header = "Bearer "+accesstoken
	xhttp.open("GET", url);
	xhttp.setRequestHeader("Authorization", bearer_header);
	// You should set responseType as blob for binary responses
	xhttp.responseType = 'blob';
	xhttp.send();
	xhttp.onload = function(e) {
        if (this.status == 200) {
            var blob = new Blob([this.response], {type:"application/octet-stream"});
            let a = document.createElement("a");
            a.style = "display: none";
            document.body.appendChild(a);
            let url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = filename;
            a.click();
            window.URL.revokeObjectURL(url);
        }else{
        //deal with your error state here
        }
     };
}
function cutFileFolder(){
	$('#pop_paste').removeClass("disabled");
	$('#pop_paste').removeClass("text-secondary");
	var filepath = $("#context-menu").data("fileInfo").path;
	var cut_list = $("#context-menu").data("cut_list")
	if(typeof cut_list == 'undefined' || cut_list == null){
		cut_list = [filepath];
	}else{
		cut_list.push(filepath);
	}
	$("#context-menu").data("cut_list",cut_list);
}
function pasteFileFolder(){
	var dest = $("#context-menu").data("fileInfo").path;
	var cut_list = $("#context-menu").data("cut_list")
	$("#context-menu").data("cut_list",null);
	var json = {}
    json['source']= cut_list;
    json['dest']= uriEncodeFileName(dest)
   $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(json),
		url: location.origin+"/moveFileFolder/",
	    success: function (data, status, jqXHR) {
	        $("#pop_paste").addClass("disabled");
			$('#pop_paste').addClass("text-secondary");
			location.reload();
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
	    }
    });
}
